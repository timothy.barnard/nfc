//
//  NFCReader.swift
//  NFC
//
//  Created by Timothy on 08/08/2019.
//  Copyright © 2019 Barnard Developments. All rights reserved.
//

import Foundation
import CoreNFC


class NFCReader: NSObject {
    
    enum NFCError: Error {
        case noData
        case tagError
        case wrongTag
        
        var description: String {
            switch self {
            case .wrongTag: return "Wrong tag type. Please use a Mifare."
            case .noData: return "No data found."
            case .tagError: return "Reading tag error. Please try again."
            }
        }
    }
    
    func writeTag(_ tag: NFCTag, message: String, completion: @escaping(_ error: NFCError?) -> Void) {
        guard case let .miFare(mifareTag) = tag, let data = message.data(using: .ascii) else {
            completion(.wrongTag)
            return
        }
        
        DispatchQueue.global().async {
            let dataOffset: UInt8 = 4
            let magicSignature: [UInt8] = [0xFE, 0x01]
            let useCount: UInt8 = 0x1
            
            let data = Data(magicSignature + [useCount, UInt8(data.count)]) + data
            
            self.writeDataTag(mifareTag, messageData: data, offset: dataOffset, completion: completion)
        }
    }
    
    func readTag(_ tag: NFCTag, completion: @escaping(_ message: String?, _ error: NFCError?) -> Void) {
        guard case let .miFare(mifareTag) = tag else {
            completion(nil, .wrongTag)
            return
        }
        DispatchQueue.global().async {
            let readBlock4: [UInt8] = [0x30, 0x04]
            let readCommand: Data = Data(readBlock4)
            self.readingTagData(mifareTag, bufferData: Data(), readCommand: readCommand, size: 16) { (data: Data?, bytesLeft: Int) in
                guard let tagData = data else {return}
                completion(tagData.toStringASCII, nil)
            }
        }
    }
    
    private func readingTagData(_ tag: NFCMiFareTag, bufferData: Data, readCommand: Data, size: Int = 16,
                                          completion: @escaping(_ data: Data?, _ bytesLeft: Int) -> Void ) {
        
        self.getTagData(tag, readCommand: readCommand, size: size) { (data: Data?, bytesLeft: Int) in
            guard let tagData = data else {
                completion(nil, 0)
                return
            }
            let buffer = bufferData + tagData
            if bytesLeft > 0 {
                let newBlock = readCommand[1] + 4
                let newReadBlock = Data([0x30, newBlock])
                self.readingTagData(tag, bufferData: buffer, readCommand: newReadBlock, size: bytesLeft, completion: completion)
            } else {
                completion(buffer, 0)
            }
        }
    }
    
    private func writeDataTag(_ tag: NFCMiFareTag, messageData: Data, offset: UInt8, completion: @escaping(_ error: NFCError?) -> Void) {
        
        // T2T Write command to write a 4 bytes block at specific block offset
        let writeBlockCommand: UInt8 = 0xA2
        let successCode: UInt8 = 0x0A
        let blockSize = 4
        var blockData: Data = messageData.prefix(blockSize)
        
        // You need to zero-padded the data to the block size
        if blockData.count < blockSize {
            blockData += Data(count: blockSize - blockData.count)
        }
        
        let writeCommand = Data([writeBlockCommand, offset]) + blockData
        
        tag.sendMiFareCommand(commandPacket: writeCommand) { (response: Data, error: Error?) in
            if error != nil {
                completion(.tagError)
                return
            }
            
            if response[0] != successCode {
                completion(.tagError)
                return
            }
            
            let newSize = messageData.count - blockSize
            if newSize > 0 {
                self.writeDataTag(tag, messageData: messageData.suffix(newSize), offset: (offset + 1), completion: completion)
            } else {
                completion(nil)
            }
        }
    }
    
    private func getTagData(_ tag: NFCMiFareTag, readCommand: Data, size: Int = 16,
                            completion: @escaping(_ data: Data?, _ bytesLeft: Int) -> Void ) {
        let firstReadCommand: Data = Data([0x30, 0x04])
        let magicSignature: [UInt8] = [0xFE, 0x01]
        let useCounterOffset = 2
        let lengthOffset = 3
        let headerLength = 4
        tag.sendMiFareCommand(commandPacket: readCommand) { (responseBlock4: Data, error: Error?) in
            if error != nil {
                completion(nil, 0)
                return
            }
            if responseBlock4.count < 16 {
                completion(nil, 0)
                return
            }
            
            // first block reading
            if firstReadCommand == readCommand {
                // Validate magic signature and use counter
                if !responseBlock4[0...1].elementsEqual(magicSignature) || responseBlock4[useCounterOffset] < 1 {
                    print("error reading singature and counter")
                    completion(nil, 0)
                    return
                }
                
                let length = Int(responseBlock4[lengthOffset])
                print("Data length: \(length)")
                
                var endBlock = 15
                if length < 16 - headerLength {
                    endBlock = headerLength + length
                }
                let bullShitBytes = responseBlock4[headerLength...endBlock]
                print("Bull shit data: \(bullShitBytes.toStringASCII!)")
                
                let remainBytes: Int = length - (16 - headerLength)
                
                completion(bullShitBytes, remainBytes)
            } else {
                if size > 16 {
                    let bytesLeft = size - 16
                    let bullShitBytes = responseBlock4[0...15]
                    print("Bull shit data: \(bullShitBytes.toStringASCII!)")
                    completion(bullShitBytes, bytesLeft)
                } else {
                    let bullShitBytes = responseBlock4[0...size-1]
                    print("Bull shit data: \(bullShitBytes.toStringASCII!)")
                    completion(bullShitBytes, 0)
                }
            }
        }
    }
    
    private func padOutData(_ data: Data) -> Data {
        var blockData: Data = data.prefix(4)
        if blockData.count < 4 {
            blockData += Data(count: 4 - blockData.count)
        }
        return blockData
    }
}

extension Data {
    var toStringASCII: String? {
        return String(bytes: self, encoding: .ascii)
    }
}

//
//  ViewController.swift
//  NFC
//
//  Created by Timothy on 08/08/2019.
//  Copyright © 2019 Barnard Developments. All rights reserved.
//

import UIKit
import CoreNFC

class ViewController: UIViewController {
    
    // MARK: - Local variables
    private lazy var messageTextField: UITextField = {
        let textField = UITextField(frame: .zero)
        textField.textAlignment = .center
        textField.delegate = self
        textField.translatesAutoresizingMaskIntoConstraints = false
        textField.placeholder = "Enter message"
        return textField
    }()
    
    private lazy var scanNFCButton: UIButton = {
        let button = UIButton(frame: .zero)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setTitle("SCAN", for: .normal)
        button.setTitleColor(.white, for: .normal)
        button.backgroundColor = UIColor.blue
        button.addTarget(self, action: #selector(scanButtonPressed(_:)), for: .touchUpInside)
        return button
    }()
    
    private lazy var writeNFCButton: UIButton = {
        let button = UIButton(frame: .zero)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setTitle("WRITE", for: .normal)
        button.setTitleColor(.white, for: .normal)
        button.backgroundColor = UIColor.blue
        button.addTarget(self, action: #selector(writeButtonPressed(_:)), for: .touchUpInside)
        return button
    }()
    
    private var readerSession: NFCTagReaderSession?
    private var tagReader: NFCReader?
    private var isWriting: Bool = false
    
    var message: String = String()
    
    // MARK: - Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupView()
    }
    
    // MARK: private functions
    private func setupView() {
        tagReader = NFCReader()
        
        let stackView = UIStackView(frame: .zero)
        stackView.alignment = .fill
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.axis = .vertical
        stackView.spacing = 10
        self.view.addSubview(stackView)
        
        stackView.addArrangedSubview(messageTextField)
        stackView.addArrangedSubview(scanNFCButton)
        stackView.addArrangedSubview(writeNFCButton)
        
        NSLayoutConstraint.activate([
            stackView.centerXAnchor.constraint(equalTo: self.view.centerXAnchor),
            stackView.centerYAnchor.constraint(equalTo: self.view.centerYAnchor),
            stackView.trailingAnchor.constraint(equalTo: self.view.trailingAnchor),
            stackView.leadingAnchor.constraint(equalTo: self.view.leadingAnchor),
            
            messageTextField.widthAnchor.constraint(equalTo: stackView.widthAnchor),
            scanNFCButton.widthAnchor.constraint(equalTo: stackView.widthAnchor),
            scanNFCButton.heightAnchor.constraint(equalToConstant: 50),
            
            writeNFCButton.widthAnchor.constraint(equalTo: stackView.widthAnchor),
            writeNFCButton.heightAnchor.constraint(equalToConstant: 50),
        ])
    }
    
    private func initReaderSession() {
        guard NFCTagReaderSession.readingAvailable else {
            let alertController = UIAlertController(title: nil, message: "NFC is not supported on this device", preferredStyle: .alert)
            alertController.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alertController, animated: true, completion: nil)
            return
        }
        readerSession = NFCTagReaderSession(pollingOption: .iso14443, delegate: self, queue: nil)
        readerSession?.alertMessage = "Hold your device near an NFC chip"
        readerSession?.begin()
    }
    
    @objc private func scanButtonPressed(_ sender: UIButton) {
        self.isWriting = false
        self.initReaderSession()
    }
    
    @objc private func writeButtonPressed(_ sender: UIButton) {
        self.isWriting = true
        self.initReaderSession()
    }

}

// MARK: - NFCTagReaderSessionDelegate
extension ViewController: NFCTagReaderSessionDelegate {
    func tagReaderSessionDidBecomeActive(_ session: NFCTagReaderSession) {
        
    }
    
    func tagReaderSession(_ session: NFCTagReaderSession, didInvalidateWithError error: Error) {
        print("NFC Tag Reader Session Error: \(error.localizedDescription)")
    }
    
    func tagReaderSession(_ session: NFCTagReaderSession, didDetect tags: [NFCTag]) {
        let foundTags: [NFCTag] = tags.filter { (tag: NFCTag) -> Bool in
            if case let .miFare(mifareTag) = tags.first, mifareTag.mifareFamily == .ultralight {
                return true
            } else {
                return false
            }
        }
        
        guard let firstTag = foundTags.first else {
            session.invalidate(errorMessage: "No mifare chips found")
            print("No Mifare chips found")
            return
        }
        
        session.connect(to: firstTag) { (error: Error?) in
            if error != nil {
                session.invalidate(errorMessage: "Connection error. Please try again.")
            }
            
            if self.isWriting {
                self.tagReader?.writeTag(firstTag, message: self.message, completion: { (error: NFCReader.NFCError?) in
                    if let nfcError = error {
                        self.readerSession?.invalidate(errorMessage: nfcError.description)
                        return
                    }
                    self.readerSession?.alertMessage = "Successfully added data"
                    self.readerSession?.invalidate()
                })
            } else {
                self.tagReader?.readTag(firstTag, completion: { (message: String?, error: NFCReader.NFCError?) in
                    if let nfcError = error {
                        self.readerSession?.invalidate(errorMessage: nfcError.description)
                        return
                    }
                    self.readerSession?.alertMessage = message ?? "No Data"
                    self.readerSession?.invalidate()
                })
            }
        }
    }
}

extension ViewController: UITextFieldDelegate {
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        self.message = textField.text ?? "ERROR"
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return true
    }
}

